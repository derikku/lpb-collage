﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using littlepixelbox.kiosk.Core.Singletons;
using littlepixelbox.kiosk.Core.Utilities;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json.Linq;
using NLog;

namespace littlepixelbox.kiosk.Core.Signalr
{
    [HubName("kioskHub")]
    public class KioskHub : Hub
    {
        private readonly string _apiUrl;

        public List<string> Images;

        private readonly Logger _log = LogManager.GetLogger("littlepixelbox");
        private string _status;

        public KioskHub()
        {
            Images = new List<string>();

            _apiUrl = AppSettings.Get<string>("CollageUrl");
            _status = string.Empty;
        }

        public override Task OnConnected()
        {
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            return base.OnDisconnected(stopCalled);
        }

        public void SelectedImages(IEnumerable<string> filenames)
        {
            SelectedImageSingleton.Instance.Images = filenames;
        }

        public void GetImages()
        {
            var imgCount = 0;

            var request = (HttpWebRequest)WebRequest.Create(string.Format("{0}/images", _apiUrl));

            try
            {
                var responseStream = request.GetResponse().GetResponseStream();

                if (responseStream != null)
                {
                    responseStream.ReadTimeout = 5000;
                    responseStream.WriteTimeout = 5000;

                    JArray ja;

                    using (var reader = new StreamReader(responseStream, Encoding.Default))
                    {
                        var s = reader.ReadToEnd();
                        ja = JArray.Parse(s);
                    }

                    foreach (var t in ja)
                    {
                        Images.Add(t["Thumbnail"].ToString());
                    }

                    imgCount = ja.Count;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }

            Clients.Client(GetClientId()).imageList(Images);
            Clients.All.statusCounters(imgCount);
        }

        public void PrintImages(int copies)
        {
            var url = string.Format("{0}/print?copies={1}", _apiUrl, copies);

            var sb = new StringBuilder();
            sb.Append(url);

            foreach (var f in SelectedImageSingleton.Instance.Images)
            {
                sb.Append(string.Format("&filename={0}", f));
            }               

            var request = (HttpWebRequest)WebRequest.Create(sb.ToString());

            try
            {
                using (var responseStream = request.GetResponse().GetResponseStream())
                {
                    var encoding = Encoding.ASCII;

                    if (responseStream != null)
                    {
                        using (var reader = new StreamReader(responseStream, encoding))
                        {
                            _status = reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _status = string.Format("An error has occured.<br><br>Please notify one of the staff for assistance.<br><br><i>{0}</i>", ex.Message);
            }

            Clients.Client(GetClientId()).printStatus(_status);
        }
        
        public void EmailImages(string emailAddress)
        {
            var url = string.Format("{0}/email?address={1}", _apiUrl, emailAddress);

            var sb = new StringBuilder();
            sb.Append(url);

            foreach (var f in SelectedImageSingleton.Instance.Images)
            {
                sb.Append(string.Format("&filename={0}",f));
            }            

            var request = (HttpWebRequest)WebRequest.Create(sb.ToString());

            try
            {
                using (var responseStream = request.GetResponse().GetResponseStream())
                {
                    var encoding = Encoding.ASCII;

                    if (responseStream != null)
                    {
                        using (var reader = new StreamReader(responseStream, encoding))
                        {
                            _status = reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _status = string.Format("An error has occured.<br><br>Please notify one of the staff for assistance.<br><br><i>{0}</i>", ex.Message);
            }

            Clients.Client(GetClientId()).emailStatus(_status);
        }

        private string GetClientId()
        {
            var clientId = "";

            if (Context.QueryString["clientId"] != null)
            {
                clientId = Context.QueryString["clientId"];
            }

            if (string.IsNullOrEmpty(clientId.Trim()))
            {
                clientId = Context.ConnectionId;
            }

            return clientId;
        }
    }
}