﻿using System.IO;
using System.Linq;
using System.Net;
using littlepixelbox.kiosk.Models;

namespace littlepixelbox.kiosk.Core.Utilities
{
    internal class Utils
    {
        /// <summary>
        /// Get local IP Address of computer
        /// </summary>
        /// <returns></returns>
        public static string LocalIpAddress()
        {
            var localIp = string.Empty;
            var host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (var ip in host.AddressList.Where(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork))
            {
                localIp = ip.ToString();
            }
            return localIp;
        }

        public static WebErrorModel GetWebError(WebException wex)
        {
            var wem = new WebErrorModel();

            if (wex.Response != null)
            {
                using (var errorResponse = (HttpWebResponse)wex.Response)
                {
                    wem.StatusCode = (int)errorResponse.StatusCode;
                    wem.StatusDescription = errorResponse.StatusDescription;

                    using (var sr = new StreamReader(errorResponse.GetResponseStream()))
                    {
                        wem.Exception = sr.ReadToEnd();
                    }
                }
            }

            return wem;
        }
    }
}