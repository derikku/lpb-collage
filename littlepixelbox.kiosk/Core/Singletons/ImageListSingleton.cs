﻿using System.Collections.Generic;

namespace littlepixelbox.kiosk.Core.Singletons
{
    public sealed class ImageListSingleton
    {
        private static ImageListSingleton _instance;
        private static readonly object Padlock = new object();

        private ImageListSingleton()
        {
            Images = new List<string>();
        }

        public static ImageListSingleton Instance
        {
            get
            {
                lock (Padlock)
                {
                    return _instance ?? (_instance = new ImageListSingleton());
                }
            }
        }

        public List<string> Images { get; set; }
    }
}
