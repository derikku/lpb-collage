﻿
namespace littlepixelbox.Core.Singletons
{
    public sealed class DefaultPathSingleton
    {
        private static DefaultPathSingleton instance = null;
        private static readonly object padlock = new object();

        private string _folderPath;

        DefaultPathSingleton()
        {
            FolderPath = string.Empty;
        }

        public static DefaultPathSingleton Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DefaultPathSingleton();
                    }
                    return instance;
                }
            }
        }

        public string FolderPath
        {
            get
            {
                return _folderPath;
            }
            set
            {
                this._folderPath = value;
            }
        }
    }
}
