﻿using System.Web.Mvc;

namespace littlepixelbox.kiosk.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Index(string code, string description, string exception)
        {
            ViewBag.Code = code;
            ViewBag.Status = description;
            ViewBag.Exception = exception;

            return View();
        }
    }
}
