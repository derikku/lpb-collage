﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Mvc;
using littlepixelbox.kiosk.Core.Singletons;
using littlepixelbox.kiosk.Core.Utilities;

namespace littlepixelbox.kiosk.Controllers
{
    public class FacebookController : Controller
    {
        //private const string BaseUrl = "http://littlepixelbox.noip.me";
        private const string BaseUrl = "http://localhost:5109";
        //private static readonly string BaseUrl = AppSettings.Get<string>("BaseUrl");
        private static readonly string ApiServer = AppSettings.Get<string>("CollageUrl");

        //this is your Facebook App ID
        public readonly string ClientId = AppSettings.Get<string>("FacebookClientId");

        //this is your Secret Key
        public readonly string ClientSecret = AppSettings.Get<string>("FacebookClientSecret");

        //your redirect uri must be EXACTLY the same Uri that caused the initial authentication handshake
        public readonly string RedirectUri = string.Format("{0}/facebook/handshake/", BaseUrl);

        public readonly string Scope = "publish_actions";

        private string _status;


        [HttpGet]
        [ActionName("login")]
        public ActionResult Login()
        {
            //redirect to https://graph.facebook.com/oauth/authorize giving Facebook my application id, the request type and the redirect url
            var asd = new RedirectResult(string.Format(
                "https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}&scope={2}&display=popup",
                ClientId, RedirectUri, Scope));
            return asd;
        }

        public ActionResult Handshake(string code)
        {
            var accessToken = string.Empty;

            //after authentication, Facebook will redirect to this controller action with a QueryString parameter called "code" (this is Facebook's Session key)

            //example uri: http://www.examplewebsite.com/facebook/handshake/?code=2.DQUGad7_kFVGqKTeGUqQTQ__.3600.1273809600-1756053625|dil1rmAUjgbViM_GQutw-PEgPIg.

            //we have to request an access token from the following Uri
            var url =
                string.Format(
                    "https://graph.facebook.com/oauth/access_token?client_id={0}&redirect_uri={1}&scope={2}&code={3}&client_secret={4}",
                    ClientId, RedirectUri, Scope, code, ClientSecret);

            //Create a webrequest to perform the request against the Uri
            var request = WebRequest.Create(url);

            //read out the response as a utf-8 encoding and parse out the access_token
            try
            {
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    if (response != null)
                    {
                        var encoding = Encoding.GetEncoding("utf-8");

                        using (var sr = new StreamReader(response.GetResponseStream(), encoding))
                        {
                            accessToken = sr.ReadToEnd().Replace("access_token=", "");
                            accessToken = accessToken.Substring(0, accessToken.IndexOf("&", StringComparison.Ordinal));
                        }
                    }
                }
            }
            catch (WebException wex)
            {
                var e = Utils.GetWebError(wex);
                return RedirectToAction("Index", "Error", new { code = e.StatusCode, description = e.StatusDescription, exception = e.Exception });
            }

            //set the access token to some session variable so it can be used through out the session
            //Session["FacebookAccessToken"] = accessToken;

            if (!string.IsNullOrEmpty(accessToken))
            {
                url = string.Format("{0}/facebook?accessToken={1}", ApiServer, accessToken);

                var sb = new StringBuilder();
                sb.Append(url);

                foreach (var f in SelectedImageSingleton.Instance.Images)
                {
                    sb.Append(string.Format("&filename={0}", f));
                }

                request = (HttpWebRequest)WebRequest.Create(sb.ToString());

                try
                {
                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        if (response != null)
                        {
                            var encoding = Encoding.ASCII;

                            using (var reader = new StreamReader(response.GetResponseStream(), encoding))
                            {
                                _status = reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (WebException wex)
                {
                    var e = Utils.GetWebError(wex);
                    return RedirectToAction("Index", "Error", new { code = e.StatusCode, description = e.StatusDescription, exception = e.Exception });
                }
            }

            return RedirectToAction("Index", "Home", new { status = _status });
        }
    }
}
