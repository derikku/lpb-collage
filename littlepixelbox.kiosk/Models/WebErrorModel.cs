﻿namespace littlepixelbox.kiosk.Models
{
    public class WebErrorModel
    {
        public int StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public string Exception { get; set; }
    }
}