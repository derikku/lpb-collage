﻿using System.Linq;
using littlepixelbox.collage.Core.Singletons;
using Nancy;
using Newtonsoft.Json;
using littlepixelbox.collage.Collage.Controllers;

namespace littlepixelbox.collage.Collage.Modules
{
    public class CollageModule : NancyModule
    {
        public CollageModule()
        {
            Get["/collage"] = x => View["Collage/Views/Collage"];

            Get["/kiosk"] = x => View["Collage/Views/Kiosk"];

            //Get["/images"] = x => GetImages();

            //Get["/print"] = x => Print(Request.Query["copies"], Request.Query["filename"]);

            //Get["/email"] = x => Email(Request.Query["address"], Request.Query["filename"]);

            //Get["/facebook"] = x => Facebook(Request.Query["accessToken"], Request.Query["filename"]);
        }

        //private static string GetImages()
        //{
        //    return JsonConvert.SerializeObject(CollageSingleton.Instance.Images, Formatting.None);
        //}

        //private static object Print(int copies, string filenames)
        //{
        //    var names = filenames.Split(',').ToArray();
        //    return ActionsController.PrintImages(copies, names);
        //}

        //private static string Email(string emailAddress, string filenames)
        //{
        //    var names = filenames.Split(',').ToArray();
        //    return ActionsController.EmailImages(emailAddress, names);
        //}

        //private static string Facebook(string accessToken, string filenames)
        //{
        //    var names = filenames.Split(',').ToArray();
        //    return ActionsController.FacebookImages(accessToken, names);
        //}
    }
}
