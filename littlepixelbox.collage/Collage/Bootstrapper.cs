﻿using Nancy;
using Nancy.Conventions;

namespace littlepixelbox.collage.Collage
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory(@"/Collage/Content"));

            base.ConfigureConventions(nancyConventions);
        }
    }
}   
