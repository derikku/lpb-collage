﻿using System;
using System.Dynamic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using Facebook;
using littlepixelbox.collage.Core;
using littlepixelbox.collage.Core.Helpers;
using littlepixelbox.collage.Core.Singletons;
using littlepixelbox.collage.Core.Utilities;
using NLog;

namespace littlepixelbox.collage.Collage.Controllers
{
    internal class ActionsController
    {
        //private static readonly Logger Log = LogManager.GetLogger("Collage");
        //public static readonly string OriginalImageFolder = @"Collage\Content\jpegs";
        //public static readonly string ThumbnailFolder = @"Collage\Content\jpegs\thumbnails";

        //private static PrintPolaroidService _pps;
        //private static string _status;

        //public static string PrintImages(int copies, string[] filenames)
        //{
        //    _pps = new PrintPolaroidService(PrinterSingleton.Instance.PrinterSettings,
        //        PrinterSingleton.Instance.MarginX,
        //        PrinterSingleton.Instance.MarginY);

        //    try
        //    {
        //        foreach (var filename in filenames)
        //        {
        //            var imgPath = string.Format("{0}\\{1}", OriginalImageFolder, filename);

        //            Log.Info("Sending {0} to printer...", filename);

        //            for (var i = 0; i < copies; i++)
        //            {
        //                _pps.PrintImageFromPath(imgPath);
        //            }

        //            Log.Info(string.Format("{0} photo(s) has been sent to printer.", copies));
        //        }

        //        _status = "Your photo(s) is now printing...";
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex.ToString());

        //        Mediator.NotifyColleagues("ErrorCount", 1);
        //        Mediator.NotifyColleagues("ErrorMessage", ex.Message);

        //        _status = string.Format("An error has occured.<br><br>Please notify one of the staff for assistance.<br><br><i>{0}</i>", ex.Message);
        //    }
        //    return _status;
        //}

        //public static string EmailImages(string mailTo, string[] filenames)
        //{
        //    try
        //    {
        //        if (IsValidEmail(mailTo))
        //        {
        //            using (var mail = new MailMessage())
        //            {
        //                var mailaddress = new MailAddress(AppSettings.Get<string>("MailServerEmailAddress"),
        //                    AppSettings.Get<string>("MailServerEmailName"));
        //                mail.Subject = "Little Pixel Box";
        //                mail.From = mailaddress;
        //                mail.To.Add(new MailAddress(mailTo));

        //                foreach (var filename in filenames)
        //                {
        //                    var imgPath = string.Format("{0}\\{1}", OriginalImageFolder, filename);

        //                    var att = new Attachment(imgPath)
        //                    {
        //                        ContentType = { MediaType = "image/jpg" }
        //                    };

        //                    mail.Attachments.Add(att);
        //                }

        //                var body = new StringBuilder();

        //                var template = Properties.Resources.email_template;

        //                body.Append(template);

        //                body.Replace("#LOGO_SOURCE#", AppSettings.Get<string>("EmailTemplate-LogoSource"));
        //                body.Replace("#LOGO_LINK#", AppSettings.Get<string>("EmailTemplate-LogoLink"));
        //                body.Replace("#CONTENT#", AppSettings.Get<string>("EmailTemplate-Content").Replace(@"\n", "<br/>"));
        //                body.Replace("#YEAR#", DateTime.Now.Year.ToString());
        //                body.Replace("#USER_EMAIL#", mailTo);
        //                body.Replace("#COMPANY_NAME#", AppSettings.Get<string>("EmailTemplate-CompanyName"));
        //                body.Replace("#COMPANY_EMAIL#", AppSettings.Get<string>("EmailTemplate-CompanyEmailAddress"));

        //                mail.Body = Regex.Replace(body.ToString(), @"\t|\n|\r", @"");
        //                mail.IsBodyHtml = true;

        //                using (
        //                    var smtp = new SmtpClient(AppSettings.Get<string>("MailServerAddress"),
        //                        AppSettings.Get<int>("MailServerPort")))
        //                {
        //                    if (AppSettings.Get<bool>("MailServerAuthentication"))
        //                    {
        //                        var smtpUserInfo = new NetworkCredential(AppSettings.Get<string>("MailServerUsername"),
        //                            AppSettings.Get<string>("MailServerPassword"));
        //                        smtp.UseDefaultCredentials = false;
        //                        smtp.Credentials = smtpUserInfo;
        //                        smtp.EnableSsl = AppSettings.Get<bool>("MailServerUseSSL");
        //                    }
        //                    Log.Info("Emailing photo(s) to {0}", mailTo);
        //                    smtp.Send(mail);

        //                    Log.Info("Email sent successfully.");
        //                    _status = string.Format("Email has been successfully sent to {0}", mailTo);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            _status = "Please enter a valid email address.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex.ToString());

        //        Mediator.NotifyColleagues("ErrorCount", 1);
        //        Mediator.NotifyColleagues("ErrorMessage", ex.Message);

        //        _status = string.Format("An error has occured.<br><br>Please notify one of the staff for assistance.<br><br><i>{0}</i>", ex.Message);
        //    }

        //    return _status;
        //}

        //public static string FacebookImages(string accessToken, string[] filenames)
        //{
        //    foreach (var filename in filenames)
        //    {
        //        var imgPath = string.Format("{0}\\{1}", ThumbnailFolder, filename);

        //        if (File.Exists(imgPath))
        //        {
        //            var client = new FacebookClient(accessToken);

        //            dynamic parameters = new ExpandoObject();
        //            parameters.subject = "Little Pixel Box";
        //            //parameters.message = "Having fun with Little Pixel Box";
        //            parameters.picture =
        //                new FacebookMediaObject
        //                {
        //                    ContentType = "image/jpeg",
        //                    FileName = DateTime.UtcNow.ToString("ddmmyyyyhhmmss") + "-photo.jpeg",
        //                }.SetValue(File.ReadAllBytes(imgPath));
        //            //parameters.caption = "Caption for the link";
        //            //parameters.description = "Longer description of the link";

        //            try
        //            {
        //                client.Post("/me/photos", parameters);
        //                _status = "Posted on Facebook successfully.";
        //            }
        //            catch (Exception ex)
        //            {
        //                _status = ex.Message;

        //                Mediator.NotifyColleagues("ErrorCount", 1);
        //                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
        //            }
        //        }
        //        else
        //        {
        //            _status = string.Format("Unable to find image {0}.", filename);
        //        }
        //    }

        //    return _status;
        //}

        //private static bool IsValidEmail(string email)
        //{
        //    try
        //    {
        //        var addr = new MailAddress(email);
        //        return addr.Address == email;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}
    }
}
