﻿using System.Collections.Generic;
using System.Linq;
using Dapper;
using littlepixelbox.collage.Core;
using littlepixelbox.collage.Core.Singletons;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using NLog;

namespace littlepixelbox.collage.Collage.SignalRHubs
{
    [HubName("collageHub")]
    public class CollageHub : Hub
    {
        private readonly Logger _log = LogManager.GetLogger("Collage");

        public CollageHub()
        {
        }

        public void GetCollageSettings()
        {
            Clients.All.settings(CollageSingleton.Instance.ImgRow, CollageSingleton.Instance.ScrollSpeed);
        }

        public void GetImages()
        {
            List<string> images;

            using (var conn = Database.GetConnection())
            {
                const string queryString = "SELECT Thumbnail FROM Data";
                images = conn.Query<string>(queryString).ToList();
            }

            Clients.Client(GetClientId()).imageList(images, ReloadClients);
            Clients.All.statusCounters(images.Count, CollageSingleton.Instance.PrintCount, CollageSingleton.Instance.ErrorCount);

            ReloadClients = false;
        }

        private string GetClientId()
        {
            var clientId = "";
            if (Context.QueryString["clientId"] != null)
            {
                clientId = Context.QueryString["clientId"];
            }

            if (string.IsNullOrEmpty(clientId.Trim()))
            {
                clientId = Context.ConnectionId;
            }

            return clientId;
        }

        private static bool ReloadClients
        {
            get { return CollageSingleton.Instance.ReloadClients; }
            set { CollageSingleton.Instance.ReloadClients = value; }
        }
    }
}