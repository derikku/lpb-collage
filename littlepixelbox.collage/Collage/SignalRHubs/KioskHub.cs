﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dapper;
using Facebook;
using littlepixelbox.collage.Core;
using littlepixelbox.collage.Core.Helpers;
using littlepixelbox.collage.Core.Singletons;
using littlepixelbox.collage.Core.Utilities;
using littlepixelbox.collage.Properties;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using NLog;

namespace littlepixelbox.collage.Collage.SignalRHubs
{
    [HubName("kioskHub")]
    public class KioskHub : Hub
    {
        private readonly Logger _log = LogManager.GetLogger("Collage");
        private static readonly string AssemblyPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

        private readonly string _baseUrl;
        private string _status;

        private PrintPolaroidService _pps;

        public KioskHub()
        {
            _baseUrl = AppSettings.Get<int>("CollagePort") == 80
                ? string.Format("http://{0}", Utils.LocalIpAddress())
                : string.Format("http://{0}:{1}", Utils.LocalIpAddress(), AppSettings.Get<string>("CollagePort"));

            _status = string.Empty;

            _pps = new PrintPolaroidService(PrinterSingleton.Instance.PrinterSettings,
                PrinterSingleton.Instance.MarginX,
                PrinterSingleton.Instance.MarginY);
        }

        public override Task OnConnected()
        {
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            return base.OnDisconnected(stopCalled);
        }

        public void SelectedImages(IEnumerable<string> filenames)
        {
            SelectedImageSingleton.Instance.Images = filenames;
        }

        //public void GetImages()
        //{
        //    var images = new List<string>();

        //    var request = (HttpWebRequest)WebRequest.Create(string.Format("{0}/{1}", _baseUrl, "images"));

        //    try
        //    {
        //        var responseStream = request.GetResponse().GetResponseStream();
        //        if (responseStream != null)
        //        {
        //            responseStream.ReadTimeout = 5000;
        //            responseStream.WriteTimeout = 5000;

        //            JArray ja;
        //            using (var reader = new StreamReader(responseStream, Encoding.Default))
        //            {
        //                var s = reader.ReadToEnd();
        //                ja = JArray.Parse(s);
        //            }

        //            images.AddRange(ja.Select(t => t["Thumbnail"].ToString()));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _log.Error(ex);

        //        Mediator.NotifyColleagues("ErrorCount", 1);
        //        Mediator.NotifyColleagues("ErrorMessage", ex.Message);
        //    }

        //    Clients.Client(GetClientId()).imageList(images, ReloadClients);
        //    Clients.All.statusCounters(images.Count, CollageSingleton.Instance.PrintCount, CollageSingleton.Instance.ErrorCount);

        //    ReloadClients = false;
        //}

        public void GetImages()
        {
            List<string> images;

            using (var conn = Database.GetConnection())
            {
                const string queryString = "SELECT Thumbnail FROM Data";
                images = conn.Query<string>(queryString).ToList();
            }

            Clients.Client(GetClientId()).imageList(images, ReloadClients);
            Clients.All.statusCounters(images.Count,  CollageSingleton.Instance.PrintCount, CollageSingleton.Instance.ErrorCount);

            ReloadClients = false;
        }

        public void PrintImages(int copies)
        {
            _pps = new PrintPolaroidService(PrinterSingleton.Instance.PrinterSettings,
                PrinterSingleton.Instance.MarginX,
                PrinterSingleton.Instance.MarginY);

            try
            {
                foreach (var filename in SelectedImageSingleton.Instance.Images)
                {
                    var imgPath = string.Format(@"{0}\Collage\Content\Jpegs\{1}", AssemblyPath, filename);

                    _log.Info("Sending {0} to printer...", filename);

                    for (var i = 0; i < copies; i++)
                    {
                        _pps.PrintImageFromPath(imgPath);
                    }

                    _log.Info(string.Format("{0} photo(s) has been sent to printer.", copies));
                }

                _status = "Your photo(s) is now printing...";
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);

                _status =
                    string.Format(
                        "An error has occured.<br><br>Please notify one of the staff for assistance.<br><br><i>{0}</i>",
                        ex.Message);
            }

            Clients.Client(GetClientId()).printStatus(_status);
        }

        public async Task EmailImages(string mailTo)
        {
            try
            {
                if (IsValidEmail(mailTo))
                {
                    using (var mail = new MailMessage())
                    {
                        var mailaddress = new MailAddress(AppSettings.Get<string>("MailServerEmailAddress"),
                            AppSettings.Get<string>("MailServerEmailName"));
                        mail.Subject = "Little Pixel Box";
                        mail.From = mailaddress;
                        mail.To.Add(new MailAddress(mailTo));

                        foreach (var filename in SelectedImageSingleton.Instance.Images)
                        {
                            var imgPath = string.Format(@"{0}\Collage\Content\Jpegs\{1}", AssemblyPath, filename);

                            var att = new Attachment(imgPath)
                            {
                                ContentType = { MediaType = "image/jpg" }
                            };

                            mail.Attachments.Add(att);
                        }

                        var body = new StringBuilder();

                        var template = Resources.email_template;

                        body.Append(template);

                        body.Replace("#LOGO_SOURCE#", AppSettings.Get<string>("EmailTemplate-LogoSource"));
                        body.Replace("#LOGO_LINK#", AppSettings.Get<string>("EmailTemplate-LogoLink"));
                        body.Replace("#CONTENT#",
                            AppSettings.Get<string>("EmailTemplate-Content").Replace(@"\n", "<br/>"));
                        body.Replace("#YEAR#", DateTime.Now.Year.ToString());
                        body.Replace("#USER_EMAIL#", mailTo);
                        body.Replace("#COMPANY_NAME#", AppSettings.Get<string>("EmailTemplate-CompanyName"));
                        body.Replace("#COMPANY_EMAIL#", AppSettings.Get<string>("EmailTemplate-CompanyEmailAddress"));

                        mail.Body = Regex.Replace(body.ToString(), @"\t|\n|\r", @"");
                        mail.IsBodyHtml = true;

                        using (
                            var smtp = new SmtpClient(AppSettings.Get<string>("MailServerAddress"),
                                AppSettings.Get<int>("MailServerPort")))
                        {
                            if (AppSettings.Get<bool>("MailServerAuthentication"))
                            {
                                var smtpUserInfo = new NetworkCredential(AppSettings.Get<string>("MailServerUsername"),
                                    AppSettings.Get<string>("MailServerPassword"));
                                smtp.UseDefaultCredentials = false;
                                smtp.Credentials = smtpUserInfo;
                                smtp.EnableSsl = AppSettings.Get<bool>("MailServerUseSSL");
                            }
                            _log.Info("Emailing photo(s) to {0}", mailTo);
                            await smtp.SendMailAsync(mail);

                            _log.Info("Email sent successfully.");
                            _status = string.Format("Email has been successfully sent to {0}", mailTo);
                        }
                    }
                }
                else
                {
                    _status = "Please enter a valid email address.";
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());

                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);

                _status =
                    string.Format(
                        "An error has occured.<br><br>Please notify one of the staff for assistance.<br><br><i>{0}</i>",
                        ex.Message);
            }

            Clients.Client(GetClientId()).emailStatus(_status);
        }

        public void FacebookImages(string accessToken)
        {
            foreach (var filename in SelectedImageSingleton.Instance.Images)
            {
                var imgPath = string.Format(@"{0}\Collage\Content\Jpegs\{1}", AssemblyPath, filename);

                if (File.Exists(imgPath))
                {
                    var client = new FacebookClient(accessToken);

                    dynamic parameters = new ExpandoObject();
                    parameters.subject = "Little Pixel Box";
                    //parameters.message = "Having fun with Little Pixel Box";
                    parameters.picture =
                        new FacebookMediaObject
                        {
                            ContentType = "image/jpeg",
                            FileName = DateTime.UtcNow.ToString("ddmmyyyyhhmmss") + "-photo.jpeg",
                        }.SetValue(File.ReadAllBytes(imgPath));
                    //parameters.caption = "Caption for the link";
                    //parameters.description = "Longer description of the link";

                    try
                    {
                        client.Post("/me/photos", parameters);
                        _status = "Posted on Facebook successfully.";
                    }
                    catch (Exception ex)
                    {
                        _status = ex.Message;

                        Mediator.NotifyColleagues("ErrorCount", 1);
                        Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                    }
                }
                else
                {
                    _status = string.Format("Unable to find image {0}.", filename);
                }
            }

            Clients.Client(GetClientId()).facebookStatus(_status);
        }

        private string GetClientId()
        {
            var clientId = "";
            if (Context.QueryString["clientId"] != null)
            {
                clientId = Context.QueryString["clientId"];
            }

            if (string.IsNullOrEmpty(clientId.Trim()))
            {
                clientId = Context.ConnectionId;
            }

            return clientId;
        }

        private static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private static bool ReloadClients
        {
            get { return CollageSingleton.Instance.ReloadClients; }
            set { CollageSingleton.Instance.ReloadClients = value; }
        }
    }
}