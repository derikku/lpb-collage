﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace littlepixelbox.collage.Views
{
    public partial class CollageTabView
    {
        public CollageTabView()
        {
            InitializeComponent();
            SetCollagePreviewImg(SliderImgRow.Value);
        }

        private void btnCopyFolder_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                IsFolderPicker = true
            };
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                TxtCopyFolder.Text = dialog.FileName;
            }
        }

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SetCollagePreviewImg(SliderImgRow.Value);
        }

        private void sliderScrollSpeed_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SetCollagePreviewImg(SliderImgRow.Value);
        }

        private void SetCollagePreviewImg(double imgPerRow)
        {
            if (SpCollagePreview == null) return;
            SpCollagePreview.Children.Clear();

            var size = SpCollagePreview.Width / imgPerRow;

            for (var i = 0; i < imgPerRow; i++)
            {
                var img = new Image
                {
                    Height = size,
                    Width = size,
                    Source = new BitmapImage(new Uri(string.Format("/Resources/Collage/{0}.jpg", i), UriKind.Relative))
                };

                SpCollagePreview.Children.Add(img);
            }

            var duration = TimeSpan.FromSeconds((CanvasCollagePreview.Height + size) / SliderScrollSpeed.Value);

            AnimationCollagePreview.To = -size;
            AnimationCollagePreview.Duration = duration;

            // Restart Storyboard
            SbCollagePreview.Stop();
            SbCollagePreview.Begin();
        }

        private void BtnOpenExportFolder_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer.exe", TxtCopyFolder.Text);
        }
    }
}
