﻿using System.Windows.Controls;
using System.Windows.Input;

namespace littlepixelbox.collage.Views
{
    public partial class StatusBarView
    {
        public StatusBarView()
        {
            InitializeComponent();
        }

        private void StatusBarItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TxtBlockErrorMessage.Text = string.Empty;
        }
    }
}
