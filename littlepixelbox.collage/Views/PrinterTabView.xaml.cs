﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using littlepixelbox.collage.Core.Singletons;
using UserControl = System.Windows.Controls.UserControl;

namespace littlepixelbox.collage.Views
{
    public partial class PrinterTabView : UserControl
    {
        public PrinterTabView()
        {
            InitializeComponent();
        }

        private void BtnDeleteAll_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (Directory.Exists(@"Collage\Content\Jpegs"))
                {
                    Directory.Delete(@"Collage\Content\Jpegs", true);
                }

                if (File.Exists("Data.sdf"))
                {
                    File.Delete("Data.sdf");
                }

                if (Directory.Exists("Logs"))
                    Directory.Delete("Logs", true);

                CollageSingleton.Instance.Images.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BtnRootFolder_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Process.Start("explorer.exe", AppDomain.CurrentDomain.BaseDirectory);
        }

        private void BtnLogFolder_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Process.Start("explorer.exe", "Logs");
        }
    }
}
