﻿using System.IO;

namespace littlepixelbox.collage.Views
{
    public partial class MainView
    {
        public MainView()
        {
            InitializeComponent();

            if (Directory.Exists(@"Collage\Content\Jpegs"))
            {
                Directory.Delete(@"Collage\Content\Jpegs", true);
            }

            if (File.Exists("Data.sdf"))
            {
                File.Delete("Data.sdf");
            }
        }
    }
}
