﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Threading;
using Dapper;
using littlepixelbox.collage.Collage;
using littlepixelbox.collage.Core;
using littlepixelbox.collage.Core.Helpers;
using littlepixelbox.collage.Core.Singletons;
using littlepixelbox.collage.Core.Utilities;
using littlepixelbox.collage.Models;
using Microsoft.Owin.Hosting;
using NLog;
using ExifLib;

namespace littlepixelbox.collage.ViewModels
{
    internal class CollageTabViewModel : ViewModelBase
    {
        private readonly Logger _log = LogManager.GetLogger("Collage");
        private bool _enableGui = true;

        private string _copyDirectory;
        private readonly string _baseUrl;
        private string _collageUrl;
        private string _kioskUrl;

        private int _imgPerRow = 5;
        private int _scrollSpeed = 20;

        private IDisposable _webApp;

        private readonly BackgroundWorker _bgWrkStartCollage = new BackgroundWorker();
        private readonly BackgroundWorker _bgWrkCopyImages = new BackgroundWorker();

        private readonly DispatcherTimer _timer = new DispatcherTimer();

        public CollageTabViewModel()
        {         
            //_copyDirectory = @"C:\Users\Derrick\Desktop\LPB";

            // Hook up background worker
            _bgWrkStartCollage.DoWork += bgWrkStartCollage_DoWork;
            _bgWrkCopyImages.DoWork += bgWrkCopyImages_DoWork;

            // Hook up the timer
            _timer.Tick += timer_Tick;
            _timer.Interval = TimeSpan.FromSeconds(5);

            // Set the default values for CollageSingleton
            CollageSingleton.Instance.ImgRow = _imgPerRow;
            CollageSingleton.Instance.ScrollSpeed = _scrollSpeed;

            int? port = !string.IsNullOrEmpty(AppSettings.Get<string>("CollagePort")) ? AppSettings.Get<int>("CollagePort") : 80;

            _collageUrl = port == 80 ? string.Format("http://{0}/collage", Utils.LocalIpAddress()) : string.Format("http://{0}:{1}/collage", Utils.LocalIpAddress(), port);
            _kioskUrl = port == 80 ? string.Format("http://{0}/kiosk", Utils.LocalIpAddress()) : string.Format("http://{0}:{1}/kiosk", Utils.LocalIpAddress(), port);

            _baseUrl = string.Format("http://{0}:{1}", Utils.LocalIpAddress(), AppSettings.Get<string>("CollagePort"));

            StartCommand = new DelegateCommand(
                () =>
                {
                    Database.CreateDatabase();

                    _timer.Start();
                    _bgWrkStartCollage.RunWorkerAsync();

                    EnableGui = false;
                    Mediator.NotifyColleagues("Status", "Running...");
                },
                () => !string.IsNullOrEmpty(_copyDirectory));

            StopCommand = new DelegateCommand(
                () =>
                {
                    if (_webApp != null)
                    {
                        _webApp.Dispose();
                        _timer.Stop();

                        EnableGui = true;

                        CollageSingleton.Instance.Images.Clear();

                        _log.Info("Collage stopped.");
                        Mediator.NotifyColleagues("Status", "Ready");
                    }
                },
                () => true);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (!_bgWrkCopyImages.IsBusy)
            {
                _bgWrkCopyImages.RunWorkerAsync();
            }
        }

        private void bgWrkStartCollage_DoWork(object sender, DoWorkEventArgs e)
        {
            var pixelBoxCollagePort = AppSettings.Get<string>("CollagePort");
            var url = string.Format("http://+:{0}/", pixelBoxCollagePort);

            try
            {
                _webApp = WebApp.Start<Startup>(url);
                _log.Info("Collage started.");
            }
            catch (TargetInvocationException tiex)
            {
                if (tiex.HResult == -2146232828) //Access is denied
                {
                    var args = string.Format(@"http add urlacl url={0} user=everyone", url);
                    var psi = new ProcessStartInfo("netsh", args)
                    {
                        Verb = "runas",
                        CreateNoWindow = true,
                        WindowStyle = ProcessWindowStyle.Hidden,
                        UseShellExecute = true
                    };

                    try
                    {
                        var process = Process.Start(psi);
                        if (process != null) process.WaitForExit();
                        _webApp = WebApp.Start<Startup>(url);
                    }
                    catch (Win32Exception ex)
                    {
                        var err = ex.NativeErrorCode == 1223
                            ? "Please select Yes to add the HTTP URL reservation."
                            : ex.Message;

                        _log.Error(err);

                        Mediator.NotifyColleagues("ErrorCount", 1);
                        Mediator.NotifyColleagues("ErrorMessage", err);
                    }
                    catch (Exception ex)
                    {
                        _timer.Stop();
                        EnableGui = true;

                        _log.Error(ex.ToString());

                        Mediator.NotifyColleagues("ErrorCount", 1);
                        Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                    }
                }
                else
                {
                    _timer.Stop();
                    EnableGui = true;
                    _log.Error(tiex.InnerException.Message);
                }
            }
            catch (Exception ex)
            {
                _timer.Stop();
                EnableGui = true;

                _log.Error(ex.ToString());

                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
            }
        }

        private void bgWrkCopyImages_DoWork(object sender, DoWorkEventArgs e)
        {
            const string collageDir = @"Collage\Content\Jpegs";
            const string thumbnailDir = @"Collage\Content\jpegs\thumbnails";

            Directory.CreateDirectory(collageDir);
            Directory.CreateDirectory(thumbnailDir);

            var copyImgFiles = Directory.EnumerateFiles(_copyDirectory, "*.*", SearchOption.TopDirectoryOnly)
                .Where(s => s.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase) || s.EndsWith(".png", StringComparison.OrdinalIgnoreCase));

            var collageImgFiles = Directory.EnumerateFiles(collageDir, "*.*", SearchOption.TopDirectoryOnly)
                .Where(s => s.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase) || s.EndsWith(".png", StringComparison.OrdinalIgnoreCase));

            foreach (var img in copyImgFiles)
            {
                var filename = Path.GetFileName(img);

                if (filename != null)
                {
                    var thumbLocation = Path.Combine(thumbnailDir, filename);
                    var imgLocation = Path.Combine(collageDir, filename);

                    var imgFiles = collageImgFiles as IList<string> ?? collageImgFiles.ToList();

                    if (!imgFiles.Select(Path.GetFileName).Contains(filename))
                    {

                        if (!Utils.IsFileLocked(new FileInfo(img)))
                        {
                            try
                            {
                                File.Copy(img, imgLocation, true);

                                // Rotate the image according to EXIF data
                                var exif = new ExifReader(img);

                                var thumb = CreateThumbnail(Image.FromFile(img));

                                ushort o = 0;
                                exif.GetTagValue<ushort>(ExifTags.Orientation, out o);

                                if (o != 0)
                                {
                                    RotateFlipType flip = OrientationToFlipType(o.ToString());

                                    if (flip != RotateFlipType.RotateNoneFlipNone)
                                        // don't flip of orientation is correct
                                    {
                                        thumb.RotateFlip(flip);
                                    }
                                }
                                thumb.Save(thumbLocation, ImageFormat.Jpeg);

                                using (var conn = Database.GetConnection())
                                {
                                    var queryString = "INSERT INTO Data VALUES (@Filename, @FileLocation, @Thumbnail)";
                                    conn.Query<ImageModel>(queryString, new
                                    {
                                        Filename = filename,
                                        FileLocation = imgLocation,
                                        Thumbnail = thumbLocation
                                    });
                                }

                                if (!CollageSingleton.Instance.Images.Any(f => f.Filename.Equals(filename)))
                                {
                                    CollageSingleton.Instance.Images.Add(new ImageModel
                                    {
                                        Filename = filename,
                                        FileLocation = _baseUrl + "/" + imgLocation.Replace(@"\", "/"),
                                        Thumbnail = _baseUrl + "/" + thumbLocation.Replace(@"\", "/")
                                    });
                                }
                            }
                            catch (Exception ex)
                            {
                                _log.Error(ex.ToString());

                                Mediator.NotifyColleagues("ErrorCount", 1);
                                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                            }
                        }
                    }
                }
            }
        }


        internal Bitmap CreateThumbnail(Image originalImage)
        {
            const int desiredWidth = 640;

            var newPixelWidth = originalImage.Width;
            var newPixelHeight = originalImage.Height;

            if (newPixelWidth > desiredWidth)
            {
                var resizePercent = desiredWidth/(float) originalImage.Width;

                newPixelWidth = (int) (originalImage.Width*resizePercent) + 1;
                newPixelHeight = (int) (originalImage.Height*resizePercent) + 1;
            }

            var bitmap = new Bitmap(newPixelWidth, newPixelHeight);

            using (var graphics = Graphics.FromImage(bitmap))
            {
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.DrawImage(originalImage, 0, 0, newPixelWidth, newPixelHeight);
            }

            return bitmap;
        }

        private static RotateFlipType OrientationToFlipType(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return RotateFlipType.RotateNoneFlipNone;
                case 2:
                    return RotateFlipType.RotateNoneFlipX;
                case 3:
                    return RotateFlipType.Rotate180FlipNone;
                case 4:
                    return RotateFlipType.Rotate180FlipX;
                case 5:
                    return RotateFlipType.Rotate90FlipX;
                case 6:
                    return RotateFlipType.Rotate90FlipNone;
                case 7:
                    return RotateFlipType.Rotate270FlipX;
                case 8:
                    return RotateFlipType.Rotate270FlipNone;
                default:
                    return RotateFlipType.RotateNoneFlipNone;
            }
        }

        public bool EnableGui
        {
            get { return _enableGui; }
            set
            {
                _enableGui = value;
                NotifyPropertyChange("EnableGui");
            }
        }

        public string CopyDirectory
        {
            get { return _copyDirectory; }
            set
            {
                _copyDirectory = value;

                NotifyPropertyChange("CopyDirectory");

                if (Directory.Exists(_copyDirectory))
                {
                    StartCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public string CollageUrl
        {
            get { return _collageUrl; }
            set
            {
                _collageUrl = value;
                NotifyPropertyChange("CollageUrl");
            }
        }

        public string KioskUrl
        {
            get { return _kioskUrl; }
            set
            {
                _kioskUrl = value;
                NotifyPropertyChange("KioskUrl");
            }
        }

        public int ImgPerRow
        {
            get { return _imgPerRow; }
            set
            {
                _imgPerRow = value;
                CollageSingleton.Instance.ImgRow = value;
                NotifyPropertyChange("ImgPerRow");
            }
        }

        public int ScrollSpeed
        {
            get { return _scrollSpeed; }
            set
            {
                _scrollSpeed = value;
                CollageSingleton.Instance.ScrollSpeed = value;
                NotifyPropertyChange("ScrollSpeed");
            }
        }

        public DelegateCommand StartCommand { get; private set; }
        public DelegateCommand StopCommand { get; private set; }
    }
}
