﻿using System.Collections.ObjectModel;
using System.Drawing.Printing;
using littlepixelbox.collage.Core.Singletons;
using NLog;

namespace littlepixelbox.collage.ViewModels
{
    public class PrinterTabViewModel : ViewModelBase
    {
        private readonly Logger _log = LogManager.GetLogger("PrintingService");
        private bool _enableGui = true;

        private string _printer;
        private readonly ObservableCollection<string> _printers = new ObservableCollection<string>();

        private PaperSize _paperSize;
        private readonly ObservableCollection<PaperSize> _paperSizes = new ObservableCollection<PaperSize>();

        private string _pageOrientation;
        private readonly ObservableCollection<string> _pageOrientations = new ObservableCollection<string>();

        private readonly PrinterSettings _printerSettings = new PrinterSettings();
        private int _marginX;
        private int _marginY;

        public PrinterTabViewModel()
        {
            _log.Trace("Finding all Printers...");
            foreach (string ps in PrinterSettings.InstalledPrinters)
            {
                _printers.Add(ps);
                _log.Trace("Added: " + ps);
            }

            // Select default printer
            Printer = _printerSettings.PrinterName;

            // Add page orientations
            PageOrientations.Add("Landscape");
            PageOrientations.Add("Portrait");

            SyncDefaultPrinterSettings();
        }


        private void SyncDefaultPrinterSettings()
        {
            _paperSizes.Clear();

            // Set the selected printer
            _printerSettings.PrinterName = Printer;

            // Add printers supported media sizes
            _log.Trace("Finding printer Media Size capabilities...");
            if (_printerSettings.PaperSizes.Count != 0)
            {
                // Add printers supported paper sizes
                _log.Trace("Finding printer Paper Sizes...");
                foreach (PaperSize size in _printerSettings.PaperSizes)
                {
                    _paperSizes.Add(size);
                }

                // Select the default paper size
                _log.Trace("Finding default Paper Size...");
                foreach (var ps in _paperSizes)
                {
                    if (_printerSettings.DefaultPageSettings.PaperSize.PaperName.Equals(ps.PaperName))
                    {
                        PaperSize = ps;
                        _log.Trace("Default Page Size: " + ps.PaperName);
                        break;
                    }
                }
            }

            // Set the default orientation
            PageOrientation = _printerSettings.DefaultPageSettings.Landscape ? "Landscape" : "Portrait";
        }

        public bool EnableGui
        {
            get { return _enableGui; }
            set
            {
                _enableGui = value;
                NotifyPropertyChange("EnableGui");
            }
        }

        public string Printer
        {
            get { return _printer; }
            set
            {
                _printer = value;
                PrinterSingleton.Instance.PrinterSettings.PrinterName = _printer;
                SyncDefaultPrinterSettings();
                NotifyPropertyChange("Printer");
            }
        }

        public ObservableCollection<string> Printers
        {
            get { return _printers; }
        }

        public PaperSize PaperSize
        {
            get { return _paperSize; }
            set
            {
                _paperSize = value;
                PrinterSingleton.Instance.PrinterSettings.DefaultPageSettings.PaperSize = value;
                NotifyPropertyChange("PaperSize");
            }
        }

        public ObservableCollection<PaperSize> PaperSizes
        {
            get { return _paperSizes; }
        }

        public string PageOrientation
        {
            get { return _pageOrientation; }
            set
            {
                _pageOrientation = value;
                PrinterSingleton.Instance.PrinterSettings.DefaultPageSettings.Landscape = value.Equals("Landscape");
                NotifyPropertyChange("PageOrientation");
            }
        }

        public ObservableCollection<string> PageOrientations
        {
            get { return _pageOrientations; }
        }

        public int MarginX
        {
            get { return _marginX; }
            set
            {
                _marginX = value;
                PrinterSingleton.Instance.MarginX = value;
                NotifyPropertyChange("MarginX");
            }
        }

        public int MarginY
        {
            get { return _marginY; }
            set
            {
                _marginY = value;
                PrinterSingleton.Instance.MarginY = value;
                NotifyPropertyChange("MarginY");
            }
        }
    }
}
