﻿using littlepixelbox.collage.Core.Helpers;
using littlepixelbox.collage.Core.Singletons;
using littlepixelbox.collage.Core.Utilities;

namespace littlepixelbox.collage.ViewModels
{
    internal class StatusBarViewModel : ViewModelBase
    {
        private string _statusText = "Ready";
        private string _errorMessage = string.Empty;
        private int _printCount;
        private int _errorCount;

        private bool _errorVisible;

        public StatusBarViewModel()
        {
            Mediator.Register("Status", UpdateStatusText);
            Mediator.Register("PrintCount", UpdatePrintCount);
            Mediator.Register("ErrorCount", UpdateErrorCount);
            Mediator.Register("ErrorMessage", UpdateErrorMessage);
        }

        public void UpdateStatusText(object args)
        {
            StatusText = (string) args;
        }

        public void UpdatePrintCount(object args)
        {
            PrintCount = (int) args;
            CollageSingleton.Instance.PrintCount = PrintCount;
        }

        public void UpdateErrorCount(object args)
        {
            ErrorCount = (int) args;
            CollageSingleton.Instance.ErrorCount = ErrorCount;
        }

        public void UpdateErrorMessage(object args)
        {
            ErrorMessage = Utils.RemoveLineEndings((string) args);
        }

        #region NotifyPropertyChange

        public string StatusText
        {
            get { return _statusText; }
            set
            {
                if (_statusText.Equals("Ready"))
                {
                    _printCount = 0;
                    NotifyPropertyChange("PrintCount");

                    _errorCount = 0;
                    NotifyPropertyChange("ErrorCount");

                    ErrorVisible = false;

                    ErrorMessage = string.Empty;
                }
                _statusText = value;
                NotifyPropertyChange("StatusText");
            }
        }

        public int PrintCount
        {
            get { return _printCount; }
            set
            {
                _printCount += value;
                NotifyPropertyChange("PrintCount");
            }
        }

        public int ErrorCount
        {
            get { return _errorCount; }
            set
            {
                if (value > 0)
                {
                    ErrorVisible = true;
                }
                _errorCount += value;
                NotifyPropertyChange("ErrorCount");
            }
        }

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    ErrorVisible = true;
                }
                _errorMessage = value;
                NotifyPropertyChange("ErrorMessage");
            }
        }

        public bool ErrorVisible
        {
            get { return _errorVisible; }
            set
            {
                _errorVisible = value;
                NotifyPropertyChange("ErrorVisible");
            }
        }

        #endregion
    }
}
