﻿namespace littlepixelbox.collage.Models
{
    public class ImageModel
    {
        public string Filename { get; set; }
        public string FileLocation { get; set; }
        public string Thumbnail { get; set; }
    }
}
