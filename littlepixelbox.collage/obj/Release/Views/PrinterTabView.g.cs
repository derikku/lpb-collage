﻿#pragma checksum "..\..\..\Views\PrinterTabView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "1C8CE6DD1692877127AAE4409AE65957"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;
using littlepixelbox.collage.ViewModels;


namespace littlepixelbox.collage.Views {
    
    
    /// <summary>
    /// PrinterTabView
    /// </summary>
    public partial class PrinterTabView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 32 "..\..\..\Views\PrinterTabView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CmbPrinterSelection;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\Views\PrinterTabView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CmbPaperSize;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\Views\PrinterTabView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CmbOrientation;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\Views\PrinterTabView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.IntegerUpDown IntUpDownMarginX;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\Views\PrinterTabView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.IntegerUpDown IntUpDownMarginY;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\Views\PrinterTabView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnDeleteAll;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\Views\PrinterTabView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnRootFolder;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\Views\PrinterTabView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnLogFolder;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/littlepixelbox.collage;component/views/printertabview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\PrinterTabView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.CmbPrinterSelection = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 2:
            this.CmbPaperSize = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 3:
            this.CmbOrientation = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 4:
            this.IntUpDownMarginX = ((Xceed.Wpf.Toolkit.IntegerUpDown)(target));
            return;
            case 5:
            this.IntUpDownMarginY = ((Xceed.Wpf.Toolkit.IntegerUpDown)(target));
            return;
            case 6:
            this.BtnDeleteAll = ((System.Windows.Controls.Button)(target));
            
            #line 69 "..\..\..\Views\PrinterTabView.xaml"
            this.BtnDeleteAll.Click += new System.Windows.RoutedEventHandler(this.BtnDeleteAll_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.BtnRootFolder = ((System.Windows.Controls.Button)(target));
            
            #line 70 "..\..\..\Views\PrinterTabView.xaml"
            this.BtnRootFolder.Click += new System.Windows.RoutedEventHandler(this.BtnRootFolder_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.BtnLogFolder = ((System.Windows.Controls.Button)(target));
            
            #line 71 "..\..\..\Views\PrinterTabView.xaml"
            this.BtnLogFolder.Click += new System.Windows.RoutedEventHandler(this.BtnLogFolder_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

