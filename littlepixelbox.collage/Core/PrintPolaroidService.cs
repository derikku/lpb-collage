﻿using System;
using System.Drawing.Printing;
using System.IO;
using System.Printing;
using System.Windows.Media.Imaging;
using littlepixelbox.collage.Core.Helpers;
using littlepixelbox.collage.Core.Utilities;
using NLog;

namespace littlepixelbox.collage.Core
{
    public class PrintPolaroidService
    {
        private readonly Logger _log = LogManager.GetLogger("PrintService");

        private readonly PrintDocument _printdocument = new PrintDocument();
        private readonly float _pageWidth;
        private readonly float _pageHeight;
        private readonly int _marginX;
        private readonly int _marginY;

        public PrintPolaroidService(PrinterSettings settings, int marginX, int marginY)
        {
            PrintController printController = new StandardPrintController();
            _printdocument.PrintController = printController;
            _printdocument.PrinterSettings = settings;

            _pageWidth = _printdocument.DefaultPageSettings.Landscape
                ? _printdocument.DefaultPageSettings.PrintableArea.Height
                : _printdocument.DefaultPageSettings.PrintableArea.Width;
            _pageHeight = _printdocument.DefaultPageSettings.Landscape
                ? _printdocument.DefaultPageSettings.PrintableArea.Width
                : _printdocument.DefaultPageSettings.PrintableArea.Height;

            _marginX = marginX != 0 ? marginX : 10;
            _marginY = marginY != 0 ? marginY : 10;
        }

        public void PrintImageFromPath(string filePath)
        {
            if (PrinterStatusOk(_printdocument.PrinterSettings.PrinterName))
            {
                var x = _marginX/2;
                var y = _marginY/2;
                var width = _pageWidth - _marginX;
                var height = _pageHeight - _marginY;

                _printdocument.DocumentName = Path.GetFileName(filePath);
                _printdocument.PrintPage +=
                    (sender, args) =>
                    {
                        BitmapSource bitmap = Utils.LoadImageUri(new Uri(filePath.Replace(@"\", "/"), UriKind.Relative));
                        args.Graphics.DrawImage(Utils.BitmapFromSource(bitmap), x, y, width, height);
                    };

                try
                {
                    _printdocument.Print();

                    Mediator.NotifyColleagues("PrintCount", (int) _printdocument.PrinterSettings.Copies);
                }
                catch (Exception ex)
                {
                    _log.Error(string.Format("Unable to print: {0}", ex));
                    Mediator.NotifyColleagues("ErrorCount", 1);
                    Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                }
            }
        }


        private bool PrinterStatusOk(string printerName)
        {
            bool isOk;

            var ps = new PrintServer();
            ps.GetPrintQueues(new[] {EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections});

            var pq = new PrintQueue(ps, printerName);

            #region Check printer status

            if (pq.HasPaperProblem)
            {
                isOk = false;
                _log.Error("Printer has paper problems.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer has paper problems.");
            }
            else if (pq.IsInError)
            {
                isOk = false;
                _log.Error("Printer is in error.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer is in error.");
            }
            else if (pq.IsPaperJammed)
            {
                isOk = false;
                _log.Error("Printer is paper jammed.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer is paper jammed.");
            }
            else if (pq.IsPaused)
            {
                isOk = false;
                _log.Error("Printer is paused.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer is paused.");
            }
            else if (pq.IsOffline)
            {
                isOk = false;
                _log.Error("Printer is offline.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer is offline.");
            }
            else if (pq.IsOutOfMemory)
            {
                isOk = false;
                _log.Error("Printer is out of memory.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer is out of memory.");
            }
            else if (pq.IsDoorOpened)
            {
                isOk = false;
                _log.Error("Printer door is opened.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer door is opened.");
            }
            else if (pq.IsTonerLow)
            {
                isOk = false;
                _log.Error("Printer toner low.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer toner low.");
            }
            else if (pq.IsOutOfPaper)
            {
                isOk = false;
                _log.Error("Printer is out of paper.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer is out of paper.");
            }
                #endregion

            else
            {
                isOk = true;
            }

            return isOk;
        }
    }
}
