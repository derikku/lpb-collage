﻿

namespace littlepixelbox.collage.Core.Wrapper
{
    public class StringValue
    {
        public StringValue(string s)
        {
            Value = s;
        }

        public string Value { get; set; }
    }
}
