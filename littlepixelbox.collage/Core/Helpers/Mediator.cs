﻿using System;
using System.Collections.Generic;

namespace littlepixelbox.collage.Core.Helpers
{
    public static class Mediator
    {
        private static readonly IDictionary<string, List<Action<object>>> PlDict =
            new Dictionary<string, List<Action<object>>>();

        public static void Register(string token, Action<object> callback)
        {
            if (!PlDict.ContainsKey(token))
            {
                var list = new List<Action<object>> {callback};
                PlDict.Add(token, list);
            }
            else
            {
                var found = false;
                for (var index = 0; index < PlDict[token].Count; index++)
                {
                    var item = PlDict[token][index];
                    if (item.Method.ToString() == callback.Method.ToString())
                        found = true;
                }
                if (!found)
                    PlDict[token].Add(callback);
            }
        }

        public static void Unregister(string token, Action<object> callback)
        {
            if (PlDict.ContainsKey(token))
                PlDict[token].Remove(callback);
        }

        public static void NotifyColleagues(string token, object args)
        {
            if (!PlDict.ContainsKey(token)) return;
            foreach (var callback in PlDict[token])
                callback(args);
        }
    }
}
