﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace littlepixelbox.Utilities
{
    class Graphics
    {
        /// <summary>
        /// Modify the Image color while ignoring transparency.
        /// </summary>
        /// <param name="image"></param>
        /// <param name="inColourR"></param>
        /// <param name="inColourG"></param>
        /// <param name="inColourB"></param>
        /// <returns></returns>
        public static BitmapImage ModifyBitmapColors(BitmapImage image, byte inColourR, byte inColourG, byte inColourB)
        {
            using (MemoryStream outStream = new MemoryStream())
            {
                PngBitmapEncoder enc = new PngBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(image));
                enc.Save(outStream);
                Bitmap bitmap = new Bitmap(outStream);

                using (Bitmap bmp = new Bitmap(outStream))
                {
                    // Specify a pixel format.
                    System.Drawing.Imaging.PixelFormat pxf = System.Drawing.Imaging.PixelFormat.Format32bppArgb;

                    // Lock the bitmap's bits.
                    Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
                    BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, pxf);

                    // Get the address of the first line.
                    IntPtr ptr = bmpData.Scan0;

                    // Declare an array to hold the bytes of the bitmap. 
                    //int numBytes = (bmp.Width * 4) * bmp.Height; 
                    int numBytes = bmpData.Stride * bmp.Height;
                    byte[] rgbValues = new byte[numBytes];

                    // Copy the RGB values into the array.
                    Marshal.Copy(ptr, rgbValues, 0, numBytes);

                    // Manipulate the bitmap
                    for (int counter = 0; counter < rgbValues.Length; counter += 4)
                    {
                        //if (rgbValues[counter] != 0 &&
                        //    rgbValues[counter + 1] != 0 &&
                        //    rgbValues[counter + 2] != 0)
                        if (rgbValues[counter + 3] != 0)
                        {
                            rgbValues[counter] = inColourB;
                            rgbValues[counter + 1] = inColourG;
                            rgbValues[counter + 2] = inColourR;
                        }
                    }

                    // Copy the RGB values back to the bitmap
                    Marshal.Copy(rgbValues, 0, ptr, numBytes);

                    // Unlock the bits.
                    bmp.UnlockBits(bmpData);

                    using (MemoryStream memory = new MemoryStream())
                    {
                        bmp.Save(memory, System.Drawing.Imaging.ImageFormat.Png);
                        memory.Position = 0;

                        BitmapImage bm = new BitmapImage();
                        bm.BeginInit();
                        bm.StreamSource = memory;
                        bm.CacheOption = BitmapCacheOption.OnLoad;
                        bm.EndInit();

                        return bm;
                    }
                }
            }
        }

        public BitmapSource ConvertToBitmapSource(UIElement element)
        {
            var target = new RenderTargetBitmap((int)(element.RenderSize.Width), (int)(element.RenderSize.Height), 96, 96, PixelFormats.Pbgra32);

            var brush = new VisualBrush(element);

            var visual = new DrawingVisual();
            var drawingContext = visual.RenderOpen();


            drawingContext.DrawRectangle(brush, null, new Rect(new Point(0, 0), new Point(element.RenderSize.Width, element.RenderSize.Height)));

            drawingContext.Close();

            target.Render(visual);

            return target;
        }   
    }
}
