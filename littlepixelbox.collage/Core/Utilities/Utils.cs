﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Windows.Media.Imaging;

namespace littlepixelbox.collage.Core.Utilities
{
    internal class Utils
    {
        /// <summary>
        ///     Get local IP Address of computer
        /// </summary>
        /// <returns></returns>
        public static string LocalIpAddress()
        {
            var localIp = string.Empty;
            var host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (var ip in host.AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork))
            {
                localIp = ip.ToString();
            }
            return localIp;
        }

        /// <summary>
        ///     Create and return BitmapImage from Uri and dispose it
        /// </summary>
        /// <param name="imageFile"></param>
        /// <returns></returns>
        public static BitmapImage LoadImageUri(Uri imageFile)
        {
            BitmapImage bitmap = null;
            if (imageFile != null)
            {
                var image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = imageFile;
                image.EndInit();

                bitmap = image;
            }
            return bitmap;
        }

        /// <summary>
        ///     Check if file is being processed by another thread
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException ex)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                var asd = ex.ToString();
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static string RemoveLineEndings(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }
            var lineSeparator = ((char) 0x2028).ToString();
            var paragraphSeparator = ((char) 0x2029).ToString();

            return
                value.Replace("\r\n", " ")
                    .Replace("\n", " ")
                    .Replace("\r", " ")
                    .Replace(lineSeparator, " ")
                    .Replace(paragraphSeparator, " ");
        }


        public static Bitmap BitmapFromSource(BitmapSource bitmapsource)
        {
            Bitmap bitmap;
            using (var outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new Bitmap(outStream);
            }
            return bitmap;
        }
    }
}