﻿using System;
using System.Data.SqlServerCe;
using System.IO;
using System.Reflection;
using Dapper;

namespace littlepixelbox.collage.Core
{
    internal class Database
    {
        private static readonly string AssemblyPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

        public static SqlCeConnection GetConnection()
        {            
            var dbPath = Path.Combine(AssemblyPath, "Data.sdf");
            var connString = string.Format("DataSource=\"{0}\";", dbPath);

            if (!File.Exists(dbPath))
            {
                CreateDatabase();
            }

            SqlCeConnection conn;

            try
            {
                conn = new SqlCeConnection(connString);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return conn;
        }

        public static void CreateDatabase()
        {
            var dbPath = Path.Combine(AssemblyPath, "Data.sdf");

            if (!File.Exists(dbPath))
            {
                var connString = string.Format("DataSource=\"{0}\";", dbPath);

                using (var en = new SqlCeEngine(connString))
                {
                    en.CreateDatabase();
                }

                const string queryString = "CREATE TABLE Data (" +
                                           "[Filename] [nvarchar](200), " +
                                           "[FileLocation] [nvarchar](200)," +
                                           "[Thumbnail] [nvarchar](200))";

                using (var conn = new SqlCeConnection(connString))
                {
                    conn.Query<int>(queryString);
                }
            }
        }
    }
}
